class Integer
  def in_words
      self.convert_to_str
  end

  def convert_to_str
    if self >= 1000
      thousands
    elsif self >= 100
      hundreds
    elsif (11..19).cover?(self)
      ten_to_twenty
    elsif self >= 10
      tens
    else
      singles
    end
  end

  def thousands
    thousands = {1_000_000_000_000=>"trillion", 1_000_000_000=>"billion",
                  1_000_000=>"million", 1_000=>"thousand"}
    core = rem = str = nil
    thousands.each do |num, name|
      if self / num >= 1
        core = self / num
        rem = self % num
        str = name
        break
      end
    end
    rem.zero? ? rem = "" : rem = " " + rem.in_words
    core.in_words + " #{str}" + rem
  end

  def hundreds
    core = self / 100
    rem = self % 100
    rem.zero? ? rem = "" : rem = " " + rem.in_words
    core.in_words + " hundred" + rem
  end

  def tens
    tens = {90=>"ninety", 80=>"eighty", 70=>"seventy", 60=>"sixty", 50=>"fifty",
            40=>"forty", 30=>"thirty", 20=>"twenty", 10=>"ten"}
    core = rem = nil
    tens.each do |num, name|
      if self / num >= 1
        core = name
        rem = self % num
        break
      end
    end
    rem.zero? ? core : core + " " + rem.in_words
  end

  def ten_to_twenty
    t_to_t = {19=>"nineteen", 18=>"eighteen", 17=>"seventeen", 16=>"sixteen",
              15=>"fifteen", 14=>"fourteen", 13=>"thirteen", 12=>"twelve",
              11=>"eleven"}
    t_to_t.each do |num, name|
      return name if self == num
    end
  end

  def singles
    singles = {9=>"nine", 8=>"eight", 7=>"seven", 6=>"six", 5=>"five",
               4=>"four", 3=>"three", 2=>"two", 1=>"one", 0=>"zero"}
    singles.each do |num, name|
      return name if self == num
    end
  end
end
